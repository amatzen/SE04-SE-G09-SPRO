# SE04-SPRO

Semester Project Group Members:

- Alexander Bundgaard Matzen <<almat20@student.sdu.dk>>
- Berkan Kütük <<bekut20@student.sdu.dk>>
- Daniel Bahrami <<dabah20@student.sdu.dk>>
- Esben Damkjær Sørensen <<essoe20@student.sdu.dk>>
- Jasan Abdikalif Farah <<jafar20@student.sdu.dk>>

## Guide for running the game
Video tutorial: https://youtu.be/L1e14msP8IY

### Requirements
- Eclipse Temurin JDK 8
- IntelliJ
- Maven
- git

### Step by step guide
1. Clone the repo
 ```git
git clone https://github.com/amatzen/SE04-SE-G09-SPRO
```

2. Change directory into the gtg directory
```terminal
cd SE04-SE-G09-SPRO/gtg
```

3. Open the project in Intellij and make sure that you are using Eclipse Temurin as JDK and Java 8 as the language

4. Run the project by opening the Maven tool window :
  - Remove under profile remove checkmark from netbeans-ide
  - Run ‘Clean’
  - Run ‘Install’
  - Run ‘pax:provision’

5. Have fun!
