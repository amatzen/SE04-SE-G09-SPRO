package dk.sdu.mmmi.swe.gtg.commoncrime;

import dk.sdu.mmmi.swe.gtg.common.data.Entity;
import dk.sdu.mmmi.swe.gtg.common.data.entityparts.TexturePart;

public interface ICrimeAction {
     void commit(Entity entity);

}
