# Controls
| Controls           | Description                   |
|--------------------|-------------------------------|
| "A" or Left arrow  | Turn left                     |
| "D" or Right arrow | Turn right                    |
| "W" or Up arrow    | Accelerate                    |
| "S" or Down arrow  | Decelerate                    |
| Space              | Shoot                         |
| "M"                | Pause/play music              |
| "K"                | Debug (Suicide)               |
| "P"                | Debug (Increase Wanted level) |
| "G"                | Debug (Enable god mode)       |
| "H"                | Debug (Refill health)         |
| "Enter"            |                               |

# Create module
`mvn pax:create-bundle "-Dpackage=dk.sdu.mmmi.swe.gtg.something" "-Dversion=1.0-SNAPSHOT"`